const html = __dirname + '/dist';

const port = process.env.PORT || 8080;
const apiUrl = '/api';

const bodyParser = require('body-parser');
const express = require('express');
var app = express();

app
    .use(bodyParser.json())
    .use(express.static(html))
    .all("/*", function(req, res, next) {
        res.sendFile("index.html", { root: __dirname + "/dist" });
    })
    .listen(port, function () {
        console.log('Port: ' + port);
        console.log('Html: ' + html);
    });